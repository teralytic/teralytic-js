# teralytic.OrganizationApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applicationCreate**](OrganizationApi.md#applicationCreate) | **POST** /organizations/{organization_id}/applications | Create a new organization client with the specified scope
[**applicationDelete**](OrganizationApi.md#applicationDelete) | **DELETE** /organizations/{organization_id}/applications/{client_id} | Delete an application api client
[**applicationList**](OrganizationApi.md#applicationList) | **GET** /organizations/{organization_id}/applications | Get organization applications
[**fieldGet**](OrganizationApi.md#fieldGet) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the organization)
[**fieldList**](OrganizationApi.md#fieldList) | **GET** /organizations/{organization_id}/fields | List all Fields associated with an organization
[**organizationGet**](OrganizationApi.md#organizationGet) | **GET** /organizations/{organization_id} | Get a specific organization
[**organizationList**](OrganizationApi.md#organizationList) | **GET** /organizations | List all Organizations
[**probeGet**](OrganizationApi.md#probeGet) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
[**probeList**](OrganizationApi.md#probeList) | **GET** /organizations/{organization_id}/probes | List all Probes associated with an organization


<a name="applicationCreate"></a>
# **applicationCreate**
> Application applicationCreate(organizationId, opts)

Create a new organization client with the specified scope

Create a new third-party application client

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization for the operation

var opts = { 
  'application': new teralytic.Application() // Application | The application to create
};
apiInstance.applicationCreate(organizationId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization for the operation | 
 **application** | [**Application**](Application.md)| The application to create | [optional] 

### Return type

[**Application**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="applicationDelete"></a>
# **applicationDelete**
> applicationDelete(organizationId, clientId)

Delete an application api client

Delete an application client for the organization from the database

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization for the operation

var clientId = "clientId_example"; // String | client_id of the application to delete

apiInstance.applicationDelete(organizationId, clientId).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization for the operation | 
 **clientId** | [**String**](.md)| client_id of the application to delete | 

### Return type

null (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="applicationList"></a>
# **applicationList**
> [Application] applicationList(organizationId)

Get organization applications

List all applications for an organization

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization for the operation

apiInstance.applicationList(organizationId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization for the operation | 

### Return type

[**[Application]**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="fieldGet"></a>
# **fieldGet**
> Field fieldGet(organizationId, fieldId)

List single Field details associated with Field id provided (from set of Fields associated with the organization)

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var fieldId = "fieldId_example"; // String | id of Field to retrieve

apiInstance.fieldGet(organizationId, fieldId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **fieldId** | [**String**](.md)| id of Field to retrieve | 

### Return type

[**Field**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="fieldList"></a>
# **fieldList**
> [Field] fieldList(organizationId, opts)

List all Fields associated with an organization

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var opts = { 
  'points': ["points_example"] // [String] | Array of points (lat,lng) describing a polygon search pattern.
};
apiInstance.fieldList(organizationId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **points** | [**[String]**](String.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional] 

### Return type

[**[Field]**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organizationGet"></a>
# **organizationGet**
> Organization organizationGet(organizationId)

Get a specific organization

List single Organization details associated with Organization id provided

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

apiInstance.organizationGet(organizationId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 

### Return type

[**Organization**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organizationList"></a>
# **organizationList**
> [Organization] organizationList()

List all Organizations

Lists all organization, requires admin scope

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();
apiInstance.organizationList().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Organization]**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="probeGet"></a>
# **probeGet**
> Probe probeGet(organizationId, probeId)

List single Probe details associated with Probe id provided (from set of Fields associated with the account key)

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var probeId = "probeId_example"; // String | id of Probe to retrieve

apiInstance.probeGet(organizationId, probeId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **probeId** | **String**| id of Probe to retrieve | 

### Return type

[**Probe**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="probeList"></a>
# **probeList**
> [Probe] probeList(organizationId, opts)

List all Probes associated with an organization

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.OrganizationApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var opts = { 
  'fields': ["fields_example"] // [String] | Fields to filter search on
};
apiInstance.probeList(organizationId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **fields** | [**[String]**](String.md)| Fields to filter search on | [optional] 

### Return type

[**[Probe]**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

