# teralytic.WeatherApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**weatherQuery**](WeatherApi.md#weatherQuery) | **GET** /weather | Get historical weather data


<a name="weatherQuery"></a>
# **weatherQuery**
> [WeatherData] weatherQuery(opts)

Get historical weather data

Query the weather data for the location

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.WeatherApi();

var opts = { 
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Start date and time for the query in RFC3339 format, the default is 24h prior 
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | End date and time for the query in RFC3339 format, the implied default is now 
  'operation': "last", // String | analytics or selection method
  'sampleRate': "15m", // String | The query sample rate
  'location': "location_example", // String | A bounding geohash of <= 5 bytes or a lat/lng string pair '-40.357589721679 175.60340881347' that will be convert to a geohash with 5 byte precision. 
  'limit': 100 // Number | Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings 
};
apiInstance.weatherQuery(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | **Date**| Start date and time for the query in RFC3339 format, the default is 24h prior  | [optional] 
 **endDate** | **Date**| End date and time for the query in RFC3339 format, the implied default is now  | [optional] 
 **operation** | **String**| analytics or selection method | [optional] [default to last]
 **sampleRate** | **String**| The query sample rate | [optional] [default to 15m]
 **location** | **String**| A bounding geohash of &lt;&#x3D; 5 bytes or a lat/lng string pair &#39;-40.357589721679 175.60340881347&#39; that will be convert to a geohash with 5 byte precision.  | [optional] 
 **limit** | **Number**| Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings  | [optional] [default to 100]

### Return type

[**[WeatherData]**](WeatherData.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

