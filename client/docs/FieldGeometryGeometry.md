# teralytic.FieldGeometryGeometry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**coordinates** | **[[[Number]]]** |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `Polygon` (value: `"Polygon"`)




