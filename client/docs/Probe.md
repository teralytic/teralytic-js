# teralytic.Probe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**serialCode** | **String** |  | [optional] 
**fieldId** | **String** |  | [optional] 
**organizationId** | **String** |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 


