# teralytic.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The users uuid | [optional] 
**organizations** | [**UUIDArray**](UUIDArray.md) |  | [optional] 
**firstName** | **String** | The user&#39;s first name | [optional] 
**lastName** | **String** | The user&#39;s last name | [optional] 
**email** | **String** | The user&#39;s email address | [optional] 
**emailVerified** | **Boolean** | Set if the user&#39;s email address is verified | [optional] 
**username** | **String** | The users login name | [optional] 
**orgRoles** | [**[OrgRole]**](OrgRole.md) |  | [optional] 
**propertyRoles** | [**[PropertyRole]**](PropertyRole.md) |  | [optional] 


