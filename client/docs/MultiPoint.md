# teralytic.MultiPoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coordinates** | [**[Point2D]**](Point2D.md) |  | [optional] 


