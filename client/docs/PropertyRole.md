# teralytic.PropertyRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Role id | [optional] 
**userId** | **String** | The user id | [optional] 
**propertyId** | **String** | The property id | [optional] 
**permissionIds** | [**UUIDArray**](UUIDArray.md) |  | [optional] 
**permissions** | [**[RolePermission]**](RolePermission.md) |  | [optional] 


