# teralytic.Feature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Object** |  | [optional] 
**type** | [**FeatureType**](FeatureType.md) |  | [optional] 
**geometry** | [**Geometry**](Geometry.md) |  | [optional] 
**bbox** | **[Number]** | Bounding box of the feature | [optional] 
**properties** | **{String: Object}** |  | [optional] 


