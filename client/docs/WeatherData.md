# teralytic.WeatherData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | **String** | Weather Summary | [optional] 
**precipType** | **String** | Precipitation Type | [optional] 
**apparentTemp** | **Number** | Apparent Temperature | [optional] 
**cloudCover** | **Number** | Cloud Cover | [optional] 
**dewPoint** | **Number** | Dew Point | [optional] 
**humidity** | **Number** | Humidity | [optional] 
**ozone** | **Number** | Ozone Rating | [optional] 
**precipIntensity** | **Number** | Precipitation Intesity | [optional] 
**precipProbability** | **Number** | Precipitation Probability | [optional] 
**pressure** | **Number** | Barometric Pressure | [optional] 
**temperature** | **Number** | Temperature | [optional] 
**time** | **Date** | Weather Sample Time (epoch) | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**uvIndex** | **Number** | UV Index | [optional] 
**windBearing** | **Number** | Wind Bearing | [optional] 
**windGust** | **Number** | Wind Gust | [optional] 
**windSpeed** | **Number** | Wind Speed | [optional] 
**visibility** | **Number** | Visibility | [optional] 


