# teralytic.GeometryType

## Enum


* `Point` (value: `"Point"`)

* `LineString` (value: `"LineString"`)

* `Polygon` (value: `"Polygon"`)

* `MultiPoint` (value: `"MultiPoint"`)

* `MultiLineString` (value: `"MultiLineString"`)

* `MultiPolygon` (value: `"MultiPolygon"`)


