# teralytic.Reading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organizationId** | **String** | The field id of the probe | [optional] 
**fieldId** | **String** | The field id of the probe | [optional] 
**probe** | [**Probe**](Probe.md) |  | [optional] 
**timestamp** | **Date** | Time when Reading was measured | [optional] 
**location** | [**Feature**](Feature.md) |  | [optional] 
**irrigation** | [**IrrigationData**](IrrigationData.md) |  | [optional] 
**microclimate** | [**MicroClimate**](MicroClimate.md) |  | [optional] 
**readings** | [**[DepthReading]**](DepthReading.md) | probe depth readings | [optional] 
**profile** | **String** | The calibration profile | [optional] 


