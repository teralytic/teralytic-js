# teralytic.SoilApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**query**](SoilApi.md#query) | **GET** /soil | Query the soildb


<a name="query"></a>
# **query**
> FeatureCollection query(geometry, opts)

Query the soildb

Query the soildb using various parameters

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.SoilApi();

var geometry = "geometry_example"; // String | The geometry to query

var opts = { 
  'component': "component_example", // String | Filter on component values
  'componentFields': ["componentFields_example"], // [String] | The component fields to return, '\\*' will return all columns, \"None\" will return return an empty object.  Default value is '\\*'
  'componentAggregation': "componentAggregation_example", // String | Component aggregation method: * `highest_percentage` - return a single component representing the highest percentage of the parent map unit 
  'horizon': "horizon_example", // String | Filter on horizons
  'horizonFields': ["horizonFields_example"], // [String] | The horizon fields to return, '\\*' will return all columns, \"None\" will return return an empty object.  Default value is '\\*'
  'horizonDepth': 56, // Number | Horizon depth to filter on
  'filter': "filter_example", // String | the named filter to use
  'geoTolerance': 10 // Number | Geometry simplification tolerance this will be inverted to 10^(10-value). A tolerance of zero can return very large (1000+ point) geometries. Use smaller tolerance with caution.  
};
apiInstance.query(geometry, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **geometry** | **String**| The geometry to query | 
 **component** | **String**| Filter on component values | [optional] 
 **componentFields** | [**[String]**](String.md)| The component fields to return, &#39;\\*&#39; will return all columns, \&quot;None\&quot; will return return an empty object.  Default value is &#39;\\*&#39; | [optional] 
 **componentAggregation** | **String**| Component aggregation method: * &#x60;highest_percentage&#x60; - return a single component representing the highest percentage of the parent map unit  | [optional] 
 **horizon** | **String**| Filter on horizons | [optional] 
 **horizonFields** | [**[String]**](String.md)| The horizon fields to return, &#39;\\*&#39; will return all columns, \&quot;None\&quot; will return return an empty object.  Default value is &#39;\\*&#39; | [optional] 
 **horizonDepth** | **Number**| Horizon depth to filter on | [optional] 
 **filter** | **String**| the named filter to use | [optional] 
 **geoTolerance** | **Number**| Geometry simplification tolerance this will be inverted to 10^(10-value). A tolerance of zero can return very large (1000+ point) geometries. Use smaller tolerance with caution.   | [optional] [default to 10]

### Return type

[**FeatureCollection**](FeatureCollection.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

