# teralytic.IrrigationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schedule** | **Number** | Irrigation Schedule | [optional] 
**volAcreIn** | **Number** | Irrigation volume in3/acre | [optional] 
**volAcreCm** | **Number** | Irrigation volume c3/acre | [optional] 
**grossIn** | **Number** | Gross irrigation in in3 | [optional] 
**grossCm** | **Number** | Gross irrigation in cm3 | [optional] 
**netIn** | **Number** | Net irrigation in in3 | [optional] 
**netCm** | **Number** | Net irrigation in cm3 | [optional] 
**recommendation** | **String** | Irrigation recommendation | [optional] 
**stream** | **Number** |  | [optional] 
**aw6** | **Number** | available water at 6 inches | [optional] 
**aw18** | **Number** | available water at 18 inches | [optional] 
**aw36** | **Number** | available water at 36 inches | [optional] 
**awTotal** | **Number** | total available water | [optional] 
**fc6** | **Number** | field capacity at 6 inches | [optional] 
**fc18** | **Number** | field capacity at 18 inches | [optional] 
**fc36** | **Number** | field capacity at 36 inches | [optional] 
**pwp6** | **Number** | Permanent Wilting Point at 6 inches | [optional] 
**pwp18** | **Number** | Permanent Wilting Point at 18 inches | [optional] 
**pwp36** | **Number** | Permanent Wilting Point at 36 inches | [optional] 


