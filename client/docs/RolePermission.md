# teralytic.RolePermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The role id | [optional] 
**domain** | **String** | The role domain | [optional] 
**action** | **String** | The role action | [optional] 


<a name="DomainEnum"></a>
## Enum: DomainEnum


* `property` (value: `"property"`)

* `org` (value: `"org"`)




<a name="ActionEnum"></a>
## Enum: ActionEnum


* `export_data` (value: `"export_data"`)

* `manage_property` (value: `"manage_property"`)

* `manage_properties` (value: `"manage_properties"`)

* `manage_team` (value: `"manage_team"`)

* `manage_orders` (value: `"manage_orders"`)




