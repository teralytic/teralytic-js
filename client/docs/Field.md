# teralytic.Field

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**organizationId** | **String** |  | [optional] 
**name** | **String** |  | 
**acreage** | **Number** |  | [optional] 
**crop** | **String** |  | [optional] 
**geometry** | [**FieldGeometry**](FieldGeometry.md) |  | [optional] 


