# teralytic.Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | The error code | [optional] 
**message** | **String** | The error message | [optional] 
**detail** | **{String: String}** | The error details | [optional] 


