# teralytic.OrgRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Role id | [optional] 
**userId** | **String** | The user id | [optional] 
**organizationId** | **String** | The organization id | [optional] 
**permissionIds** | [**UUIDArray**](UUIDArray.md) |  | [optional] 
**permissions** | [**[RolePermission]**](RolePermission.md) |  | [optional] 
**owner** | **Boolean** | Flag is the user role is an owner | [optional] 


