# teralytic.MicroClimate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**humidity** | **Number** |  | [optional] 
**lux** | **Number** |  | [optional] 
**temperature** | **Number** | Air Temperature | [optional] 
**eto** | **Number** | Evaporative Transpiration | [optional] 


