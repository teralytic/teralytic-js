# teralytic.DepthReading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depth** | **Number** | The depth value | [optional] 
**depthUnits** | **String** | The depth units (i.e. in, cm) | [optional] 
**terascore** | **Number** | TeraScore | [optional] 
**nitrogen** | **Number** | Nitrogen | [optional] 
**nitrogenPpm** | **Number** | Nitrogen ppm | [optional] 
**phosphorus** | **Number** | Phosphorus | [optional] 
**phosphorusPpm** | **Number** | Phosphorus ppm | [optional] 
**potassium** | **Number** | Potassium | [optional] 
**potassiumPpm** | **Number** | Potassium ppm | [optional] 
**ec** | **Number** | Electrical Conductivity | [optional] 
**o2** | **Number** | Dioxygen | [optional] 
**pH** | **Number** | pH Scale | [optional] 
**co2** | **Number** | Carbon Dioxide | [optional] 
**awc** | **Number** |  | [optional] 
**moisture** | **Number** | Soil Moisture | [optional] 
**temperature** | **Number** | Soil Temperature | [optional] 
**extendedAttributes** | **{String: Object}** | Additional properties | [optional] 


