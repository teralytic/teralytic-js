# teralytic.TelemetryApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**execCalibration**](TelemetryApi.md#execCalibration) | **POST** /organizations/{organization_id}/calibrations | Execute sensor calibrations
[**readingsQuery**](TelemetryApi.md#readingsQuery) | **GET** /organizations/{organization_id}/readings | Query sensor readings


<a name="execCalibration"></a>
# **execCalibration**
> [Reading] execCalibration(organizationId, opts)

Execute sensor calibrations

Execute a calibration script on a set of raw data

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.TelemetryApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var opts = { 
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Start date and time for the query in RFC3339 format, the default is 24h prior 
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | End date and time for the query in RFC3339 format, the implied default is now 
  'fields': ["fields_example"], // [String] | The fields to return readings for
  'probes': ["probes_example"], // [String] | The probes to return readings for
  'properties': ["properties_example"], // [String] | The properties to return readings for
  'limit': 100, // Number | Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings 
  'persist': false, // Boolean | publish the results to the timeseries database
  'logGroup': "logGroup_example", // String | Debug log group to log to cloudwatch
  'profileName': "profileName_example", // String | Specify the calibration profile identifier 
  'handler': "handler", // String | The calibration script hander
  'file': "/path/to/file.txt", // File | The profile source document
  'url': "url_example" // String | The profile source url
};
apiInstance.execCalibration(organizationId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **startDate** | **Date**| Start date and time for the query in RFC3339 format, the default is 24h prior  | [optional] 
 **endDate** | **Date**| End date and time for the query in RFC3339 format, the implied default is now  | [optional] 
 **fields** | [**[String]**](String.md)| The fields to return readings for | [optional] 
 **probes** | [**[String]**](String.md)| The probes to return readings for | [optional] 
 **properties** | [**[String]**](String.md)| The properties to return readings for | [optional] 
 **limit** | **Number**| Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings  | [optional] [default to 100]
 **persist** | **Boolean**| publish the results to the timeseries database | [optional] [default to false]
 **logGroup** | **String**| Debug log group to log to cloudwatch | [optional] 
 **profileName** | **String**| Specify the calibration profile identifier  | [optional] 
 **handler** | **String**| The calibration script hander | [optional] [default to handler]
 **file** | **File**| The profile source document | [optional] 
 **url** | **String**| The profile source url | [optional] 

### Return type

[**[Reading]**](Reading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="readingsQuery"></a>
# **readingsQuery**
> [Reading] readingsQuery(organizationId, startDate, opts)

Query sensor readings

Query sensor readings associated with organization

### Example
```javascript
var teralytic = require('teralytic');
var defaultClient = teralytic.ApiClient.instance;

// Configure API key authorization: ApiKeyAuth
var ApiKeyAuth = defaultClient.authentications['ApiKeyAuth'];
ApiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyAuth.apiKeyPrefix = 'Token';

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new teralytic.TelemetryApi();

var organizationId = "organizationId_example"; // String | id of Organization to retrieve

var startDate = "startDate_example"; // String | Start date and time for the query format. Specifying `first` will return the oldest available data.  Example - `2019-03-15T00:00:00-00:00` 

var opts = { 
  'endDate': "endDate_example", // String | End date and time for the query, the implied default is now  Example - `2019-03-15T00:00:00-00:00` 
  'fields': ["fields_example"], // [String] | The fields to return readings for
  'probes': ["probes_example"], // [String] | The probes to return readings for
  'location': "location_example", // String | A bounding geohash of <= 7 bytes or a lat/lng string pair '-40.357589721679 175.60340881347' that will be convert to a geohash with 7 byte precision. 
  'properties': ["properties_example"], // [String] | The properties to return readings for
  'extended': false, // Boolean | Return extended attributes
  'options': "options_example", // String | Options for the call
  'operation': "last", // String | The analytics operations to perform
  'sampleRate': "1h", // String | The query sample rate
  'fill': "none", // String | The missing value fill method 
  'limit': 24, // Number | Limit the number of readings to return per probe, the max is 7 days,  the default is 24 hours. 
  'offset': 0, // Number | offset into the query to return 
  'profile': "default", // String | Specify the calibration profile 
  'sort': "ASC" // String | Specify the sort field for the results.  * ASC - ascending and group by timestamp * DEC - decending and group by timestamp * NONE - sort by full group by tags which effectively results in sorting by organization, field, and location. 
};
apiInstance.readingsQuery(organizationId, startDate, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**String**](.md)| id of Organization to retrieve | 
 **startDate** | **String**| Start date and time for the query format. Specifying &#x60;first&#x60; will return the oldest available data.  Example - &#x60;2019-03-15T00:00:00-00:00&#x60;  | 
 **endDate** | **String**| End date and time for the query, the implied default is now  Example - &#x60;2019-03-15T00:00:00-00:00&#x60;  | [optional] 
 **fields** | [**[String]**](String.md)| The fields to return readings for | [optional] 
 **probes** | [**[String]**](String.md)| The probes to return readings for | [optional] 
 **location** | **String**| A bounding geohash of &lt;&#x3D; 7 bytes or a lat/lng string pair &#39;-40.357589721679 175.60340881347&#39; that will be convert to a geohash with 7 byte precision.  | [optional] 
 **properties** | [**[String]**](String.md)| The properties to return readings for | [optional] 
 **extended** | **Boolean**| Return extended attributes | [optional] [default to false]
 **options** | **String**| Options for the call | [optional] 
 **operation** | **String**| The analytics operations to perform | [optional] [default to last]
 **sampleRate** | **String**| The query sample rate | [optional] [default to 1h]
 **fill** | **String**| The missing value fill method  | [optional] [default to none]
 **limit** | **Number**| Limit the number of readings to return per probe, the max is 7 days,  the default is 24 hours.  | [optional] [default to 24]
 **offset** | **Number**| offset into the query to return  | [optional] [default to 0]
 **profile** | **String**| Specify the calibration profile  | [optional] [default to default]
 **sort** | **String**| Specify the sort field for the results.  * ASC - ascending and group by timestamp * DEC - decending and group by timestamp * NONE - sort by full group by tags which effectively results in sorting by organization, field, and location.  | [optional] [default to ASC]

### Return type

[**[Reading]**](Reading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/csv, application/json

