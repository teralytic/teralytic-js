# teralytic.Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The client id, if not provided one will be generated | [optional] 
**organizationId** | **String** | The organization this application belongs to | [optional] 
**name** | **String** | The application name | 
**clientSecret** | **String** | The client secret, if not provided one will be generated | [optional] 
**expiresAt** | **Date** | The client expiration date | [optional] 
**scope** | [**StringArray**](StringArray.md) |  | [optional] 


