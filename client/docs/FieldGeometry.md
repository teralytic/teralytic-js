# teralytic.FieldGeometry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**geometry** | [**FieldGeometryGeometry**](FieldGeometryGeometry.md) |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `Feature` (value: `"Feature"`)




