# teralytic.FeatureCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | [**[Feature]**](Feature.md) |  | [optional] 
**type** | **String** |  | [optional] [default to &#39;FeatureCollection&#39;]


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `FeatureCollection` (value: `"FeatureCollection"`)




