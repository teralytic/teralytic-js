/**
 * teralytic
 * The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
 *
 * OpenAPI spec version: 1.1.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.4
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.teralytic);
  }
}(this, function(expect, teralytic) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new teralytic.Field();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Field', function() {
    it('should create an instance of Field', function() {
      // uncomment below and update the code to test Field
      //var instance = new teralytic.Field();
      //expect(instance).to.be.a(teralytic.Field);
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

    it('should have the property organizationId (base name: "organization_id")', function() {
      // uncomment below and update the code to test the property organizationId
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

    it('should have the property name (base name: "name")', function() {
      // uncomment below and update the code to test the property name
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

    it('should have the property acreage (base name: "acreage")', function() {
      // uncomment below and update the code to test the property acreage
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

    it('should have the property crop (base name: "crop")', function() {
      // uncomment below and update the code to test the property crop
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

    it('should have the property geometry (base name: "geometry")', function() {
      // uncomment below and update the code to test the property geometry
      //var instance = new teralytic.Field();
      //expect(instance).to.be();
    });

  });

}));
