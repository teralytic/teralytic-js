/**
 * teralytic
 * The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
 *
 * OpenAPI spec version: 1.1.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.4
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Geometry', 'model/GeometryType', 'model/Point2D'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Geometry'), require('./GeometryType'), require('./Point2D'));
  } else {
    // Browser globals (root is window)
    if (!root.teralytic) {
      root.teralytic = {};
    }
    root.teralytic.Point = factory(root.teralytic.ApiClient, root.teralytic.Geometry, root.teralytic.GeometryType, root.teralytic.Point2D);
  }
}(this, function(ApiClient, Geometry, GeometryType, Point2D) {
  'use strict';




  /**
   * The Point model module.
   * @module model/Point
   * @version 1.1.0-dev.14
   */

  /**
   * Constructs a new <code>Point</code>.
   * GeoJSON geometry
   * @alias module:model/Point
   * @class
   * @extends module:model/Geometry
   * @param type {module:model/GeometryType} 
   */
  var exports = function(type) {
    var _this = this;
    Geometry.call(_this, type);

  };

  /**
   * Constructs a <code>Point</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Point} obj Optional instance to populate.
   * @return {module:model/Point} The populated <code>Point</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      Geometry.constructFromObject(data, obj);
      if (data.hasOwnProperty('coordinates')) {
        obj['coordinates'] = Point2D.constructFromObject(data['coordinates']);
      }
    }
    return obj;
  }

  exports.prototype = Object.create(Geometry.prototype);
  exports.prototype.constructor = exports;

  /**
   * @member {module:model/Point2D} coordinates
   */
  exports.prototype['coordinates'] = undefined;



  return exports;
}));

