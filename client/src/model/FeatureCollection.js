/**
 * teralytic
 * The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
 *
 * OpenAPI spec version: 1.1.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.4
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Feature'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Feature'));
  } else {
    // Browser globals (root is window)
    if (!root.teralytic) {
      root.teralytic = {};
    }
    root.teralytic.FeatureCollection = factory(root.teralytic.ApiClient, root.teralytic.Feature);
  }
}(this, function(ApiClient, Feature) {
  'use strict';




  /**
   * The FeatureCollection model module.
   * @module model/FeatureCollection
   * @version 1.1.0-dev.14
   */

  /**
   * Constructs a new <code>FeatureCollection</code>.
   * @alias module:model/FeatureCollection
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>FeatureCollection</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/FeatureCollection} obj Optional instance to populate.
   * @return {module:model/FeatureCollection} The populated <code>FeatureCollection</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('features')) {
        obj['features'] = ApiClient.convertToType(data['features'], [Feature]);
      }
      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/Feature>} features
   */
  exports.prototype['features'] = undefined;
  /**
   * @member {module:model/FeatureCollection.TypeEnum} type
   * @default 'FeatureCollection'
   */
  exports.prototype['type'] = 'FeatureCollection';


  /**
   * Allowed values for the <code>type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.TypeEnum = {
    /**
     * value: "FeatureCollection"
     * @const
     */
    "FeatureCollection": "FeatureCollection"  };


  return exports;
}));

