module.exports = function(grunt) {

  grunt.initConfig({
    bump: {
      options: {
        files: ['config.json'],
        updateConfigs: [],
        commit: false,
        commitMessage: 'Release v%VERSION%',
        commitFiles: ['-a'],
        createTag: true,
        tagName: 'v%VERSION%',
        tagMessage: 'Version %VERSION%',
        push: false,
        pushTo: 'origin',
        gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
        globalReplace: false,
        prereleaseName: 'dev',
        metadata: '',
        regExp: false
      }
    },
  });

  grunt.loadNpmTasks('grunt-bump');
};