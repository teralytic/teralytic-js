# teralytic-js

Teralytic Javascript (node) SDK

This build is automated using swagger-codegen and the gitlab CI / CD pipelines.

## Manual install

To manually install this client you must have the pre-reqs installed:

1. docker 2.0.0+
2. curl
3. make
4. npm

Clone the repo and run make

```bash
> git clone https://gitlab.com/teralytic/teralytic-js.git
> cd teralytic-js
> make
```
