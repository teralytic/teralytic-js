SWAGGER_SPEC ?= "https://api.swaggerhub.com/apis/Teralytic/teralytic-api/1.1.0/swagger.yaml"

default: install

fetch:
	curl -o swagger.yaml $(SWAGGER_SPEC)

build: fetch
	docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli:2.4.4 generate -l javascript -i /local/swagger.yaml -c /local/config.json -t /local/templates -o /local/client

install: build
	cd client && npm install .

.PHONY: \
	install \
	build \
	fetch